1. Books authored by Marjorie Green:
	- The Busy Executive's Database Guide
	- You Can Combat Computer Stress!

2. Books authored by Michael O'Leary:
	- Cooking with Computers

3. Author/s of "The Busy Executive's Database Guide:
	- Marjorie Green and Abraham Bennet

4. Publisher of "But Is It User Friendly?":
	- Algodata Infosystems

5. Books published by Algodata Infosystems:
	- The Busy Executive's Database Guide
	- Cooking with Computers
	- Straight Talk About Computers
	- But Is It User Friendly
	- Secrets of Silicon Valley
	- Net Etiquette